import { useRef, useState } from "react";

const UseRef = () => {
    const [texto, setTexto] = useState('');

    const inputRef = useRef<HTMLInputElement | null>(null);
    const colorRef = useRef<HTMLParagraphElement | null>(null);

    const printValue = () => {
        setTexto(inputRef.current?.value ?? '')
    }

    const changeColor = () => {
        colorRef.current && (colorRef.current.className = 'text-blue')
    }

    return (
        <>
            <span>Esto es UseRef</span>
            <p>
                <input
                    type="text"
                    ref={inputRef}
                />
            </p>
            <button onClick={printValue}>Mostrar</button>
            <p className="text-red" ref={colorRef}>{texto}</p>
            <button onClick={changeColor}>Cambiar</button>
        </>
    );
}

export default UseRef;