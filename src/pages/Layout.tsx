import { Outlet } from "react-router-dom";
import { EjemploProvider } from "../context/EjemploProvider";

const Layout = () => {
    return (
        <EjemploProvider>
            <p>Esto es Layout</p>
            <Outlet />
        </EjemploProvider>
    );
} 

export default Layout;