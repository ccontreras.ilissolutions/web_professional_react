/** Objeto para crear transacciones en transbank */
export interface IWebPayTransaction {
    buyOrder: string,
    sessionId: string,
    amount: number,
    returnUrl: string
}

/** Objeto de respuesta de transbank al crear la operación */
export interface IWebPayResponse {
    token: string,
    url: string,
}

/**Objeto de respuesta de transbank al procesar transacción */
export interface IWebPayCommitResponse {
    vci: string;
    amount: number;
    status: string;
    buyOrder: string;
    sessionId: string;
    cardDetail: {
        cardNumber: string;
    };
    accountingDate: string;
    transactionDate: string;
    authorizationCode: string;
    paymentTypeCode: string;
    responseCode: number;
    installmentsAmount: number;
    installmentsNumber: number;
    balance: number;
}