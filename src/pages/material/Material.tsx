import { NavLink } from "react-router-dom";

const Material = () => {
    return (
        <>
        <span>Esto es Material</span>
        <p><NavLink to={'/material/botones'}>Botones</NavLink></p>
        <p><NavLink to={'/material/list'}>List</NavLink></p>
        <p><NavLink to={'/material/drawer'}>Drawer</NavLink></p>
        <p><NavLink to={'/material/table'}>Table</NavLink></p>
        <p><NavLink to={'/material/accordion'}>Accordion</NavLink></p>
        <p><NavLink to={'/material/stepper'}>Stepper</NavLink></p>
        <p><NavLink to={'/material/autocomplete'}>Autocomplete</NavLink></p>
        <p><NavLink to={'/material/date-picker'}>DatePicker</NavLink></p>
        </>
    );
}

export default Material;