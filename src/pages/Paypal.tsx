import { v4 as uuidv4 } from 'uuid';
import { IWebPayTransaction } from '../domain/interfaces/IWebPay';
import { useLayoutEffect, useState } from 'react';
import { paypalCrearOrden } from '../data/services/PaypalService';
import { NavLink } from 'react-router-dom';

const Paypal = () => {
    /**Generador de ID Random 
     * type string
    */
    const uuid = uuidv4();

    /** Objeto de prueba para generar transacción */
    const mockTransaction: IWebPayTransaction = {
        buyOrder: "1",
        sessionId: uuid,
        amount: 10000.0,
        returnUrl: "http://localhost:5173/webpay/response"
    }

    const [datos, setDatos] = useState<any>()

    /**Función asincrona que crea la transacción */
    const getPayPalResponse = async (): Promise<void> => {
        try {
            const datos = await paypalCrearOrden({amount:mockTransaction.amount});
            setDatos(datos)
            return datos;
        } catch (error) {
            throw new Error(`Error al obtener la respuesta de la transacción de WebPay`);
        }
    }

    useLayoutEffect(() => {
        getPayPalResponse()
    }, [])


  return (
    <>
            <p>Esto es Paypal</p>
            <ul>
                <li><strong>Orden:</strong>{mockTransaction.buyOrder}</li>
                <li><strong>Producto:</strong> Anillo de Oro</li>
                <li><strong>Cantidad:</strong> 1</li>
                <li><strong>Monto:</strong>{mockTransaction.amount}</li>
                <li><strong>URL:</strong>{datos?.url ?? ''}</li>
                <li><strong>Orden:</strong>{datos?.orden ?? ''}</li>
            </ul>
            <NavLink to={datos?.url ?? '#'}>Pagar</NavLink> 
        </>
  )
}

export default Paypal