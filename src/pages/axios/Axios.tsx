import { Link } from "react-router-dom";

const Axios = () => {
    return (
        <>
        <span>Esto es Axios</span>
        <ul>
            <li>
                <Link to={"/axios/categorias"}>Categorias</Link>
            </li>
            <li>
                <Link to={"/axios/productos"}>Productos</Link>
            </li>
            
        </ul>
        </>
    );
}

export default Axios;