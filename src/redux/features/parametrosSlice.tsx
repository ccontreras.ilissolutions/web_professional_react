import { PayloadAction, createSlice } from "@reduxjs/toolkit";
export interface IParametros {
    nombre: string;
    perfil_id:number | null;
    perfil: string;
}

const initialState:IParametros = {
    nombre: "", 
    perfil_id: null,
    perfil: ""
}

export const parametrosSlice = createSlice({
    name: "parametros",//cuando se importa se usa este nombre con el sufijo Reducer Ej. parametrosReducer
    initialState,
    reducers:
    {
        iniciarSesion: (state:IParametros, datos:PayloadAction<IParametros>) => {
            state.nombre= datos.payload.nombre
            state.perfil_id = datos.payload.perfil_id
            state.perfil = datos.payload.perfil
        }  
    }
})

export const {iniciarSesion} = parametrosSlice.actions;

export default parametrosSlice.reducer;