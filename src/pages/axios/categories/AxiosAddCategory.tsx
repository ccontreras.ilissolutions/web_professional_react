import Swal from 'sweetalert2';
import { addCategorias } from '../../../data/services/ApiRestAxios';
import { useFormik } from 'formik';
import { useNavigate } from 'react-router-dom';

export interface IFormularioAddCategory {
    nombre: string;
}

const AxiosAddCategory = () => {

    const navigate = useNavigate()

    
    const { handleSubmit, handleChange, values } = useFormik({
        initialValues: {
            nombre: "",
        },
        onSubmit: (values: IFormularioAddCategory) => {
            let mensaje = ''

            if (!values.nombre) {
                mensaje = mensaje + "<li>Nombre de categoria obligatorio</li>"
            }
            if (mensaje === '') {
                addCategorias({nombre: values.nombre}).then((result) => {
                    if (result === 201){
                        Swal.fire({
                            icon: "success",
                            title: 'Wohoww',
                            html: `<u>'Categoria creada'</u>`
                        })
                        navigate('/axios/categorias')
                    } else{
                        Swal.fire({
                            icon: "error",
                            title: 'Uops',
                            html: `<u>'tuvimos un problema con el proceso'</u>`
                        })
                    }
                })
                
            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>${mensaje}</u>`
                })
            }
        }
    })

    return (
        <>
            <div>AxiosAddCategory</div>
            <form onSubmit={handleSubmit} >
                <div>
                    <label htmlFor="nombre">nombre</label>
                    <input id='nombre' name='nombre' value={values.nombre} onChange={handleChange}></input>
                </div>
                <hr />
                <button type="submit">Enviar</button>
            </form>
        </>
    )
}

export default AxiosAddCategory