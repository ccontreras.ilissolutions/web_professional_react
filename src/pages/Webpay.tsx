import { useEffect, useState } from 'react';
import WebpayController from '../controllers/WebPayController'
import { IWebPayResponse, IWebPayTransaction } from '../domain/interfaces/IWebPay';
import { v4 as uuidv4 } from 'uuid';

/** Componente que simula el envío de una orden a Transbank */
const Webpay = () => {

    /**Generador de ID Random 
     * type string
    */
    const uuid = uuidv4();

    /** Objeto de prueba para generar transacción */
    const mockTransaction: IWebPayTransaction = {
        buyOrder: "1",
        sessionId: uuid,
        amount: 10000.0,
        returnUrl: "http://localhost:5173/webpay/response"
    }


    /**LLamado al controlador para crear transacción */
    const {
        createWebPayTransaction
    } = WebpayController();

    /**Estado de la url de respuesta de transbank */
    const [url, setUrl] = useState<string>('');
    /**Estado del token de respuesta de transbank */
    const [token, setToken] = useState<string>('');

    /**Función asincrona que crea la transacción */
    const getWebPayResponse = async (): Promise<void> => {
        try {
            const apiResponse: IWebPayResponse | null = await createWebPayTransaction(mockTransaction);
            if (apiResponse) {
                setUrl(apiResponse.url);
                setToken(apiResponse.token);
            } else {
                throw new Error("No se pudo obtener una respuesta válida de la transacción de WebPay.");
            }
        } catch (error) {
            throw new Error(`Error al obtener la respuesta de la transacción de WebPay`);
        }
    }

    /** Caso de uso que crea la transacción al cargar el compoenente */
    useEffect(() => {
        getWebPayResponse()
    }, []);


    return (
        <>
            <p>Esto es Webpay</p>
            <ul>
                <li><strong>Orden:</strong>{mockTransaction.buyOrder}</li>
                <li><strong>Producto:</strong> Anillo de Oro</li>
                <li><strong>Cantidad:</strong> 1</li>
                <li><strong>Monto:</strong>{mockTransaction.amount}</li>
                <li><strong>URL:</strong>{url}</li>
                <li><strong>Token:</strong>{token}</li>
            </ul>
            <form action={url} method="POST">
                <input type="hidden" name="token_ws" value={token} />
                <input type="submit" value="Pagar" />
            </form>
        </>
    );
}

export default Webpay;