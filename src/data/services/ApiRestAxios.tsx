import axios from "axios"

/**
 * Endpoint para generar token:  https://www.api.tamila.cl/api/login
 * Body: {
    "correo": "info@tamila.cl",
    "password": "p2gHNiENUw"
    }
 * 
 */

const header = {
    'content-type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MzYsImlhdCI6MTcyMDczODA5NCwiZXhwIjoxNzIzMzMwMDk0fQ.uYxhc0z333yKoAJrTJr9IOiQhnjsBMb23RTQ4mxz9jc'
}

const headerUpload = {
    'content-type': 'multipart/form-data',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MzYsImlhdCI6MTcyMDczODA5NCwiZXhwIjoxNzIzMzMwMDk0fQ.uYxhc0z333yKoAJrTJr9IOiQhnjsBMb23RTQ4mxz9jc'
}


export const getCategorias = async () => {
    const datos = axios.get(`${import.meta.env.VITE_API_URL}categorias`,
        {
            headers: header
        }).then((response) => {
            if (response.status == 200) {
                return response.data;
            } else {
                console.log('Fallo comunicacion en endpoint')
            }
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

export const getCategoriasById = async (id: number) => {
    const datos = axios.get(`${import.meta.env.VITE_API_URL}categorias/${id}`,
        {
            headers: header
        }).then((response) => {
            if (response.status == 200) {
                return response.data;
            } else {
                console.log('Fallo comunicacion en endpoint')
            }
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

interface ICategoryToAdd {
    nombre: string;
}

export const addCategorias = async (request: ICategoryToAdd) => {
    const datos = axios.post(`${import.meta.env.VITE_API_URL}categorias`, request,
        {
            headers: header
        }).then((response) => {
            return response.status
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

export const editCategorias = async (request: ICategoryToAdd, id: number) => {
    const datos = axios.put(`${import.meta.env.VITE_API_URL}categorias/${id}`, request,
        {
            headers: header
        }).then((response) => {
            return response.status
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

export const deleteCategorias = async (id: number) => {
    const datos = axios.delete(`${import.meta.env.VITE_API_URL}categorias/${id}`,
        {
            headers: header
        }).then((response) => {
            return response.status
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}


export const getProductos = async (page: number) => {
    const datos = axios.get(`${import.meta.env.VITE_API_URL}productos?page=${page}`,
        {
            headers: header
        }).then((response) => {
            if (response.status == 200) {
                return response.data;
            } else {
                console.log('Fallo comunicacion en endpoint')
            }
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

export const getProductosById = async (id: number) => {
    const datos = axios.get(`${import.meta.env.VITE_API_URL}productos/${id}`,
        {
            headers: header
        }).then((response) => {
            if (response.status == 200) {
                return response.data;
            } else {
                console.log('Fallo comunicacion en endpoint')
            }
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

export const getProductosByCategory = async (category: string) => {
    const datos = axios.get(`${import.meta.env.VITE_API_URL}productos-buscar/${category}`,
        {
            headers: header
        }).then((response) => {
            if (response.status == 200) {
                return response.data;
            } else {
                console.log('Fallo comunicacion en endpoint')
            }
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}


interface IProductToAdd {
    categorias_id:number;
    nombre: string;
    descripcion: string;
    precio: number;
    stock: number;
}

export const addProductos = async (request: IProductToAdd) => {
    const datos = axios.post(`${import.meta.env.VITE_API_URL}productos`, request,
        {
            headers: header
        }).then((response) => {
            return response.status
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

export const editProductos = async (request: IProductToAdd, idProduct:number) => {
    const datos = axios.put(`${import.meta.env.VITE_API_URL}productos/${idProduct}`, request,
        {
            headers: header
        }).then((response) => {
            return response.status
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

export const deleteProductos = async (id: number) => {
    const datos = axios.delete(`${import.meta.env.VITE_API_URL}productos/${id}`,
        {
            headers: header
        }).then((response) => {
            return response.status
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}


export const getFotosDeProductos = async (id: number) => {
    const datos = axios.get(`${import.meta.env.VITE_API_URL}productos-fotos/${id}`,
        {
            headers: header
        }).then((response) => {
            if (response.status == 200) {
                return response.data;
            } else {
                console.log('Fallo comunicacion en endpoint')
            }
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

export const addFotosPorProducto = async (imagen:Blob | null, product_id:string) => {
    if (!imagen) {
        throw new Error('No se ha seleccionado ninguna imagen');
    }
    const formData = new FormData();
    formData.append('imagen', imagen)
    formData.append('productos_id', product_id)

    const datos = axios.post(`${import.meta.env.VITE_API_URL}productos-fotos`, formData,
        {
            headers: headerUpload
        }).then((response) => {
            if (response.status == 201) {
                return response.status;
            } else {
                console.log('Fallo comunicacion en endpoint')
            }
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

export const deleteFotos = async (id: number) => {
    const datos = axios.delete(`${import.meta.env.VITE_API_URL}productos-fotos/${id}`,
        {
            headers: header
        }).then((response) => {
            return response.status
        }).catch((err) => {
            console.log('Fallo conexion', err)
        });

    return datos;

}

