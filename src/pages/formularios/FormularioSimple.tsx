import { FormEvent, useState } from 'react';
import Swal from 'sweetalert2';

const FormularioSimple = () => {
  const [nombre, setNombre] = useState<string>('');

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    Swal.fire({
        title: "Formulario envíado",
        text: `Ingresaste ${nombre}`,
        icon: "info"
      });
  };
  return (
    <>
      <p>Esto es Formulario Simple</p>
      <form onSubmit={handleSubmit}>
        <div className='form-group'>
          <label htmlFor='nombre'>Nombre</label>
          <input
            type='text'
            id='nombre'
            className='form-control'
            value={nombre}
            onChange={(e) => {
              setNombre(e.target.value);
            }}
          />
        </div>
        <hr />
        <button type='submit'>Enviar</button>
      </form>
    </>
  );
};

export default FormularioSimple;
