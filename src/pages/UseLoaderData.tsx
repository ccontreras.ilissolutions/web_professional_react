import { useLoaderData } from "react-router-dom";
import { paises } from "../mocks/dataUseLoaderData";

export const loader = () => {
    const countries = paises;
    return countries;
}

const UseLoaderData = () => {

    const countries = useLoaderData();

    console.log(countries)

    return (
        <>
            <p>Esto es UseLoaderData</p>
        </>
    );
}

export default UseLoaderData;