import { LeadingActions, SwipeAction, SwipeableList, SwipeableListItem, TrailingActions } from "react-swipeable-list";
import "react-swipeable-list/dist/styles.css"
const ReactSwipeableList = () => {

    const leadingActions = () => (
        <LeadingActions>
            <SwipeAction destructive onClick={() => {console.log('eliminar')}}><h1>Eliminar</h1></SwipeAction>
        </LeadingActions>
    )
    const trailingActions = () => (
        <TrailingActions>
                        <SwipeAction  onClick={() => {console.log('archivar')}}><h1>Archivar</h1></SwipeAction>
        </TrailingActions>
     )


    return (
        <>
        <span>Esto es ReactSwipeableList</span>
        <SwipeableList>
            <SwipeableListItem leadingActions={leadingActions()} trailingActions={trailingActions()}>
                <div>
                    <h1>{`<-.......Prueba.........->`}</h1>
                </div>
            </SwipeableListItem>
        </SwipeableList>
        </>
    );
}

export default ReactSwipeableList;