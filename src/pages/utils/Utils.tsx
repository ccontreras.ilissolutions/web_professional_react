import { NavLink } from "react-router-dom";

const Utils = () => {
    return (
        <>
        <span>Esto es Utils</span>
        <p><NavLink to={'/utils/days'}>Days.js</NavLink></p>
        <p><NavLink to={'/utils/spinner'}>Spinner</NavLink></p>
        <p><NavLink to={'/utils/react-swipeable-list'}>React Swipeable List</NavLink></p>
        <p><NavLink to={'/utils/react-webcam'}>React Webcam</NavLink></p>
        <p><NavLink to={'/utils/mapasv1'}>Mapas Version 1</NavLink></p>
        
        </>
    );
}

export default Utils;