import { useCallback, useRef } from "react";
import Webcam from "react-webcam";

const ReactWebcam = () => {
    const webcamRef = useRef<Webcam>(null)
  const capture = useCallback(
    () => {
      const imageSrc = webcamRef.current?.getScreenshot();
      console.log(imageSrc)
    },
    [webcamRef]
  );

    const videoConstraints = {
        width: 1280,
        height: 720,
        facingMode: "user"
      };

    return (
        <>
      <Webcam
        audio={false}
        height={720}
        ref={webcamRef}
        screenshotFormat="image/jpeg"
        width={1280}
        videoConstraints={videoConstraints}
      />
      <button onClick={capture}>Capturar</button>
    </>
    );
}

export default ReactWebcam;