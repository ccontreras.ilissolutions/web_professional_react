

const paises = [
    {
        id: 1,
        nombre: 'Chile',
        dominio: 'cl'
    },
    {
        id: 2,
        nombre: 'Peru',
        dominio: 'pe'
    },
    {
        id: 3,
        nombre: 'Bolivia',
        dominio: 'bo'
    },
    {
        id: 4,
        nombre: 'Argentina',
        dominio: 'ar'
    },
    {
        id: 5,
        nombre: 'Colombia',
        dominio: 'co'
    },
    {
        id: 6,
        nombre: 'Venezuela',
        dominio: 've'
    },
    {
        id: 7,
        nombre: 'Mexico',
        dominio: 'me'
    },
    {
        id: 8,
        nombre: 'España',
        dominio: 'es'
    },
]

export { paises }