import { useEffect, useState } from 'react'
import { deleteCategorias, getCategorias } from '../../../data/services/ApiRestAxios';
import { NavLink, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';



export interface ICategory {
    id: number;
    nombre: string;
    slug: string;
}

const AxiosCategory = () => {

    const navigate = useNavigate()

    const [categories, setCategories] = useState<ICategory[] | null>(null);

    const getCategory = async () => {
        const categoryList = await getCategorias();
        setCategories(categoryList)
        return categoryList
    }

    const handleConfirmDelete = (id: number) => {
        Swal.fire({
            icon: "warning",
            title: '¿Estas seguro de eliminar esta categoria?',
            showCancelButton: true,
            cancelButtonText: 'NO',
            confirmButtonText: 'SI'
        }).then((result) => {
            if (result.isConfirmed) {
                handleDeleteCategory(id)
            }
        })
    }

    const handleDeleteCategory = async (id: number) => {

        const categoryList = await deleteCategorias(id).then((result) => {
            if (result) {
                Swal.fire({
                    icon: "success",
                    title: 'Exito',
                    html: `<u>'Categoria eliminada'</u>`
                })
                getCategory()
            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>'tuvimos un problema con el proceso'</u>`
                })
                navigate('/axios/categorias')
            }
        })
        return categoryList

    }

    useEffect(() => {
        !categories && getCategory()
    }, [categories]);
    return (
        <>
            <div>AxiosCategory</div>
            <button onClick={() => navigate('/axios/categorias/add')}>Crear +</button>
            {categories &&
                <table>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>nombre</th>
                            <th>slug</th>
                            <th>editar</th>
                            <th>eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        {categories.map((category) => (
                            <tr key={category.id}>
                                <td>{category.id}</td>
                                <td>{category.nombre}</td>
                                <td>{category.slug}</td>
                                <td><NavLink to={`/axios/categorias/edit/${category.id}`}><button>Editar</button></NavLink></td>
                                <td><button onClick={() => handleConfirmDelete(category.id)}>Eliminar</button></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            }
        </>
    )
}

export default AxiosCategory