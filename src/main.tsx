import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import './index.css';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import Home from './pages/Home.tsx';
import Nosotros from './pages/Nosotros.tsx';
import Error from './pages/Error.tsx';
import Layout from './pages/Layout.tsx';
import QueryStringPage from './pages/QueryStringPage.tsx';
import CustomHooks from './pages/CustomHooks.tsx';
import UseLoaderData, { loader as paisesLoader } from './pages/UseLoaderData.tsx';
import UseRef from './pages/useRef.tsx';
import Webpay from './pages/Webpay.tsx';
import WebPayResponse from './pages/WebPayResponse.tsx';
import Formularios from './pages/formularios/Formularios.tsx';
import FormularioSimple from './pages/formularios/FormularioSimple.tsx';
import FormularioUseActionData, { action as procesarActionData } from './pages/formularios/FormularioUseActionData.tsx';
import FormularioFormik from './pages/formularios/FormularioFormik.tsx';
import FormularioReactHookForm from './pages/formularios/FormularioReactHookForm.tsx';
import FormularioReactFinalForm from './pages/formularios/FormularioReactFinalForm.tsx';
import Utils from './pages/utils/Utils.tsx';
import Days from './pages/utils/Days.tsx';
import Spinner from './pages/utils/Spinner.tsx';
import ReactSwipeableList from './pages/utils/ReactSwipeableList.tsx';
import ReactWebcam from './pages/utils/ReactWebcam.tsx';
import MapasV1 from './pages/utils/MapasV1.tsx';
import Material from './pages/material/Material.tsx';
import MaterialBotones from './pages/material/MaterialBotones.tsx';
import MaterialList from './pages/material/MaterialList.tsx';
import MaterialDrawer from './pages/material/MaterialDrawer.tsx';
import MaterialTable from './pages/material/MaterialTable.tsx';
import MaterialAccordion from './pages/material/MaterialAccordion.tsx';
import MaterialStepper from './pages/material/MaterialStepper.tsx';
import MaterialAutocomplete from './pages/material/MaterialAutocomplete.tsx';
import MaterialDatePicker from './pages/material/MaterialDatePicker.tsx';
import Storage from './pages/localstorage/Storage.tsx';
import LocalStorage from './pages/localstorage/LocalStorage.tsx';
import SessionStorage from './pages/localstorage/SessionStorage.tsx';
import UseContext from './pages/UseContext.tsx';
import ReduxEjemplo from './pages/ReduxEjemplo.tsx';
import { Provider } from 'react-redux';
import store from './redux/store/store.tsx';
import Axios from './pages/axios/Axios.tsx';
import AxiosCategory from './pages/axios/categories/AxiosCategory.tsx';
import AxiosAddCategory from './pages/axios/categories/AxiosAddCategory.tsx';
import AxiosEditCategory from './pages/axios/categories/AxiosEditCategory.js';
import AxiosProducts from './pages/axios/products/AxiosProducts.tsx';
import AxiosProductsByCategory from './pages/axios/products/AxiosProductsByCategory.tsx';
import AxiosAddProduct from './pages/axios/products/AxiosAddProduct.tsx';
import AxiosEditProduct from './pages/axios/products/AxiosEditProduct.tsx';
import AxiosProductsPhotos from './pages/axios/products/AxiosProductsPhotos.tsx';
import Fetch from './pages/fetch/Fetch.tsx';
import FetchCategory from './pages/fetch/categories/FetchCategory.tsx';
import Paypal from './pages/Paypal.tsx';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    errorElement: <Error />,
    children: [
      {
        index: true,
        element: <Home />,
      },
      {
        path: '/sobre-nosotros',
        element: <Nosotros />,
      },
      {
        path: '/ruta/search-params', //recibe los search params ?id={@numero}&{@texto} y renderiza esos valores
        element: <QueryStringPage />,
      },
      {
        path: '/custom-hooks',
        element: <CustomHooks />,
      },
      {
        path: '/use-loader-data',
        loader: paisesLoader,
        element: <UseLoaderData />,
      },
      {
        path: '/use-ref',
        loader: paisesLoader,
        element: <UseRef />,
      },
      {
        path: '/webpay',
        element: <Webpay />,
      },
      {
        path: '/webpay/response',
        element: <WebPayResponse />,
      },
      {
        path: '/paypal',
        element: <Paypal />,
      },
      {
        path: '/formularios',
        children: [
          {
            index: true,
            element: <Formularios />,
          },
          {
            path: '/formularios/formulario-simple',
            element: <FormularioSimple />,
          },
          {
            path: '/formularios/formulario-use-action-data',
            element: <FormularioUseActionData />,
            action: procesarActionData,
          },
          {
            path: '/formularios/formulario-formik',
            element: <FormularioFormik />,
          },
          {
            path: '/formularios/formulario-react-hook-form',
            element: <FormularioReactHookForm />,
          },
          {
            path: '/formularios/formulario-react-final-form',
            element: <FormularioReactFinalForm />,
          },
        ],
      },
      {
        path: '/utils',
        children: [
          {
            index: true,
            element: <Utils />,
          },
          {
            path: '/utils/days',
            element: <Days />,
          },
          {
            path: '/utils/spinner',
            element: <Spinner />,
          },
          {
            path: '/utils/react-swipeable-list',
            element: <ReactSwipeableList />,
          },
          {
            path: '/utils/react-webcam',
            element: <ReactWebcam />,
          },
          {
            path: '/utils/mapasv1',
            element: <MapasV1 />,
          }
        ],
      },
      {
        path: '/material',
        children: [
          {
            index: true,
            element: <Material />,
          },
          {
            path: '/material/botones',
            element: <MaterialBotones />,
          },
          {
            path: '/material/list',
            element: <MaterialList />,
          },
          {
            path: '/material/drawer',
            element: <MaterialDrawer />,
          },
          {
            path: '/material/table',
            element: <MaterialTable />,
          },
          {
            path: '/material/accordion',
            element: <MaterialAccordion />,
          },
          {
            path: '/material/stepper',
            element: <MaterialStepper />,
          },
          {
            path: '/material/autocomplete',
            element: <MaterialAutocomplete />,
          },
          {
            path: '/material/date-picker',
            element: <MaterialDatePicker />,
          },
        ],
      },
      {
        path: '/storage',
        children: [
          {
            index: true,
            element: <Storage />,
          },
          {
            path: '/storage/localstorage',
            element: <LocalStorage />,
          },
          {
            path: '/storage/sessionstorage',
            element: <SessionStorage />,
          },
        ],
      },
      {
        path: '/context',
        element: <UseContext />,
      },
      {
        path: '/redux',
        element: <ReduxEjemplo />,
      },
      {
        path: '/axios',
        children: [
          {
            index: true,
            element: <Axios />,
          },
          {
            path: '/axios/categorias',
            children: [
              {
                index: true,
                element: <AxiosCategory />,
              },
              {
                path: '/axios/categorias/add',
                element: <AxiosAddCategory />,
              },
              {
                path: '/axios/categorias/edit/:idCategory',
                element: <AxiosEditCategory />,
              },
            ],
          },
          {
            path: '/axios/productos',
            children: [
              {
                index: true,
                element: <AxiosProducts />,
              },
              {
                path: '/axios/productos/productos-categoria/:slugCategory',
                element: <AxiosProductsByCategory />,
              },
              {
                path: '/axios/productos/add',
                element: <AxiosAddProduct />,
              },
              {
                path: '/axios/productos/edit/:idProduct',
                element: <AxiosEditProduct />,
              },
              {
                path: '/axios/productos/fotos-productos/:idProduct',
                element: <AxiosProductsPhotos />,
              },
            ],
          },
        ],
      },
      {
        path: '/fetch',
        children: [
          {
            index: true,
            element: <Fetch />,
          },
          {
            path: '/fetch/categorias',
            children: [
              {
                index: true,
                element: <FetchCategory />,
              },
              {
                path: '/fetch/categorias/add',
                element: <></>,
              },
              {
                path: '/fetch/categorias/edit/:idCategory',
                element: <></>,
              },
            ],
          },
          {
            path: '/fetch/productos',
            children: [
              {
                index: true,
                element: <></>,
              }],
          },
        ],
      },
    ],
  },
  {
    path: '*',
    element: <>404</>,
  },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router}></RouterProvider>
      <App />
    </Provider>
  </React.StrictMode>
);
