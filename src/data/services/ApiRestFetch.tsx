const header = {
    'content-type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MzYsImlhdCI6MTcyMjk4NzM4NiwiZXhwIjoxNzI1NTc5Mzg2fQ.Aw1Abo90OQfBM5X_jB8j6hfTJn6YLAhiWWs5jzKuL0Q'
}

const headerUpload = {
    'content-type': 'multipart/form-data',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MzYsImlhdCI6MTcyMjk4NzM4NiwiZXhwIjoxNzI1NTc5Mzg2fQ.Aw1Abo90OQfBM5X_jB8j6hfTJn6YLAhiWWs5jzKuL0Q'
}

console.log(header, headerUpload)

export const getCategorias = async () => {
    const url = `${import.meta.env.VITE_API_URL}categorias`;
    const response = await fetch(url, { headers: header });
    return response.json();
  };

  interface ICategoryToAdd {
    nombre: string;
}

  export const addCategorias = async (request: ICategoryToAdd) => {
    const url = `${import.meta.env.VITE_API_URL}categorias`;
    const response = await fetch(url, { method: 'POST', body: JSON.stringify(request), headers: header });
    return response;

}