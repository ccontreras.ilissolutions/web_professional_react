import { Field, Form } from "react-final-form";
import Swal from "sweetalert2";

interface IFormularioReactFinalForm {
    correo: string;
    password: string
}

const FormularioReactFinalForm = () => {
    const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms))
    const required = (value:IFormularioReactFinalForm) => (value? undefined: 'Este campo es obligatorio')
    const composeValidators = (...validators) => (value:IFormularioReactFinalForm) => validators.reduce((error, validator) => error || validator(value), undefined)
    const onSubmit = async (values:IFormularioReactFinalForm) => {
        await sleep(300)
        Swal.fire({
            icon: "success",
            title: 'OK',
            text: `Email: ${values.correo}`
        })
    }
    return (
        <>
            <span>Esto es FormularioReactFinalForm</span>
            <Form onSubmit={onSubmit}
                render={
                    ({handleSubmit, form, submitting, pristine, values}) => (
                        <form onSubmit={handleSubmit}>
                            {/* <Field name="correo"  id= "correo" component="input" type="email"  placeholder="email" className=''/> */}
                            <Field name="correo" validate={composeValidators(required)}>
                                {({input, meta}) => (
                                    <>
                                    <label htmlFor="correo">email</label>
                                    <input type="text" className="" placeholder="email" {...input}></input>
                                    {meta.error && meta.touched && <span>{meta.error}</span>}
                                    </>
                                )}
                            </Field>
                            <Field name="password" validate={composeValidators(required)}>
                                {({input, meta}) => (
                                    <>
                                    <label htmlFor="password">email</label>
                                    <input type="password" className="" placeholder="password" {...input}></input>
                                    {meta.error && meta.touched && <span>{meta.error}</span>}
                                    </>
                                )}
                            </Field>
                            <hr/>
                            <button type="submit" disabled={submitting || pristine} >enviar</button>
                        </form>
                    )
                }>

            </Form>
        </>

    );
}

export default FormularioReactFinalForm;