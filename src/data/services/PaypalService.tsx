export async function paypalCrearOrden(datos:any){
    const respuesta = await fetch(`${import.meta.env.VITE_API_URL}paypal`,
    {
        method: 'POST',
        body: JSON.stringify(datos),
        headers: {'content-type': 'application/json'}
    })
    return respuesta.json()
}

export async function paypalRespuesta(datos:any){
    const respuesta = await fetch(`${import.meta.env.VITE_API_URL}paypal-capture`,
    {
        method: 'POST',
        body: JSON.stringify(datos),
        headers: {'content-type': 'application/json'}
    })
    return respuesta.json()
}

export async function paypalCancelado(datos:any){
    const respuesta = await fetch(`${import.meta.env.VITE_API_URL}paypal-cancelar`,
    {
        method: 'POST',
        body: JSON.stringify(datos),
        headers: {'content-type': 'application/json'}
    })
    return respuesta.json()
}