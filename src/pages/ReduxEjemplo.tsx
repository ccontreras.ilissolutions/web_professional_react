import {useDispatch, useSelector} from 'react-redux'
import { IProcedencia, cambiarChile, cambiarMexico, cambiarPeru, volverVenezuela } from '../redux/features/procedenciaSlice';
import { RootState } from '../redux/store/store';
import { ICalculadora, dividir, multiplicar, resetear, restar, sumar } from '../redux/features/calculadoraSlice';
import { IParametros, iniciarSesion } from '../redux/features/parametrosSlice';
const ReduxEjemplo = () => {

    const procedencia:IProcedencia = useSelector((state:RootState) => (state.procedencia) )
    const calculadora:ICalculadora = useSelector((state:RootState) => (state.calculadora) )
    const parametros: IParametros = useSelector((state:RootState) => (state.parametros))
    const dispatch = useDispatch();
    return (
        <>
        <span>Esto es ReduxEjemplo</span>
        <ul>
            <li>Pais:{ procedencia.country}</li>
            <li>Ciudad:{ procedencia.city}</li>

        </ul>
        <button onClick={() => dispatch(cambiarMexico())}>Cambiar a Mexico</button>
        <button onClick={() => dispatch(cambiarPeru())}>Cambiar a Peru</button>
        <button onClick={() => dispatch(cambiarChile())}>Cambiar a Chile</button>
        <button onClick={() => dispatch(volverVenezuela())}>Volver a Venezuela</button>


        <hr></hr>

        <span>Calculadora</span>
        <p>Primer numero: {calculadora.numberA}</p>
        <p>segundo numero:{calculadora.numberB}</p>
        <button onClick={() => dispatch(sumar())}>Suma</button>
        <button onClick={() => dispatch(restar())}>Resta</button>
        <button onClick={() => dispatch(multiplicar())}>Multiplicar</button>
        <button onClick={() => dispatch(dividir())}>Dividir</button>
        <button onClick={() => dispatch(resetear())}>Resetear</button>
        <p>Resultado: {calculadora.resultado}</p>

        <hr></hr>

        <span>Parametros</span>
        <ul>
            <li>Nombre: {parametros.nombre}</li>
            <li>ID: {parametros.perfil_id}</li>
            <li>Perfil: {parametros.perfil}</li>
        </ul>
        <button onClick={() => {dispatch(iniciarSesion({nombre: "Carlos", perfil_id: 13, perfil: "Admin"}))}}>Iniciar Sesión</button>



        </>
    );
}

export default ReduxEjemplo;