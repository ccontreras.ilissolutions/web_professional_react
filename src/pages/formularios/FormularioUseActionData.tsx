import { Form , useActionData} from 'react-router-dom';
import Swal from 'sweetalert2';
import { atributos, categorias_productos } from '../../mocks/dataUseActionData';
import { useState } from 'react';

export async function action({ request }:any) {

  const formData = await request.formData()
  const datos = Object.fromEntries(formData);

  const errores:string[] = [];

  if (formData.get('categoria') === '0') {
    errores.push('debe seleccionar al menos una categoria')
  }
  if (Object.values(datos).includes('')) {
    errores.push('todos los campos deben ser obligatorios')
  }

  const expresion_precio = new RegExp("[0-9]")
  if(!expresion_precio.test(formData.get('precio'))){
    errores.push('precio errado')
  }

  if(Object.keys(errores).length){
    return errores;
  }

  //recibir checkbox dinamicos
  const arreglo = []
  let mensajeArreglo = '';
atributos.map((atributo, index) => {
 if(formData.get(`atribute${atributo.id}`) !=null)
  {
    arreglo[index]= atributo.id;
    mensajeArreglo=mensajeArreglo + atributo.nombre + ','
  }
})

  return Swal.fire({
    icon: 'info',
    title: 'ok',
    text: `los datos enviados son: Nombre: ${datos.nombre}, Precio: ${datos.precio}, Categoría: ${datos.categoria}, Destacado: ${datos.destacado}, Descripción: ${datos.descripcion} | atributos: ${mensajeArreglo}`,
  });
}

const FormularioUseActionData = () => {

  const errores = useActionData();

  //ejemplo validacion asincrona
  const [name, setName] = useState<string>('');
 const handleBlur = async(name:string) => {
    if(name === 'Carlos'){
      Swal.fire({
        icon: 'error',
        title: 'Ops',
        text: 'este nombre es del admin usa otro',
      });
      setName('')
    }
  }
  return (
    <>
      <p>Esto es Formulario UseActionData</p>
      <ul>
        {errores && errores.map((error, index) => (
          <li key={index}> {error}</li>
        ))}
      </ul>
      <Form method='POST' noValidate>
        <div className='form-group'>
          <label htmlFor='categoria'>Categoria</label>
          <select name='categoria' className='form-control'>
            <option value={'0'}>Seleccione </option>
            {categorias_productos.map((categoria) => (
              <option key={categoria.id} value={categoria.id}>
                {categoria.nombre}
              </option>
            ))}
          </select>
        </div>
        <div className='form-group'>
          <label htmlFor='nombre'>Nombre</label>
          <input type='text' name='nombre' className='form-control' 
          value={name}
          onChange={(e) => {setName(e.target.value)}}
          onBlur={(e) => handleBlur(e.target.value)}/>
        </div>
        <div className='form-group'>
          <label htmlFor='precio'>Precio</label>
          <input type='number' min='0' name='precio' className='form-control' />
        </div>
        <hr />
        <div className='form-control'>
          <label htmlFor='destacado'>destacado</label>
          <div>
            <input type='radio' id='destacado1' name='destacado' value={'si'} />
            <label htmlFor='destacado1'>si</label>
          </div>
          <div>
            <input type='radio' id='destacado2' name='destacado' value={'no'} />
            <label htmlFor='destacado2'>no</label>
          </div>
        </div>
        <div className='form-group'>
          <label htmlFor='descripcion'>Descripcion</label>
          <textarea name='descripcion' className='form-control' />
        </div>
        <hr />
        <div className='form-group'>
          <label htmlFor='atributos'>Atributos</label>
          {atributos.map((atributo) => (
            <div key={atributo.id}>
                      <input  type='checkbox' name={`atribute${atributo.id}`} 
                      value={atributo.id}
                      className='form-control' />
                      <label htmlFor='ssss'>{atributo.nombre}</label>
            </div>
          ))}
          
        </div>
        <hr />

        <button>Enviar</button>
      </Form>
    </>
  );
};

export default FormularioUseActionData;
