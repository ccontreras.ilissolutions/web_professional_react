const categorias = [
    {
        id: 1,
        nombre: 'Frutas'
    },
    {
        id: 2,
        nombre: 'Verduras'
    }
]


const productos = [
    {
        id: 1,
        categoryId: 1,
        nombre: 'Manzana',
        precio: 1000
    },
    {
        id: 2,
        categoryId: 1,
        nombre: 'Uvas',
        precio: 123
    },
    {
        id: 3,
        categoryId: 1,
        nombre: 'Duraznos',
        precio: 245
    },
    {
        id: 4,
        categoryId: 2,
        nombre: 'Papas',
        precio: 790
    },
    {
        id: 5,
        categoryId: 2,
        nombre: 'Lechuga',
        precio: 1200
    },
    {
        id: 6,
        categoryId: 2,
        nombre: 'Repollo',
        precio: 1800
    },
    {
        id: 7,
        categoryId: 2,
        nombre: 'Pimenton',
        precio: 840
    },
    {
        id: 8,
        categoryId: 1,
        nombre: 'Limon',
        precio: 1120
    },
]

export { categorias, productos }