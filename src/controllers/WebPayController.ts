
import WebPayRepository from '../data/repositories/WebPayRepository';
import { IWebPayCommitResponse, IWebPayResponse, IWebPayTransaction } from '../domain/interfaces/IWebPay';


/** Representa el controlador que maneja los eventos relacionados a webpay
  * @returns Funciones del controlador
*/
const WebPayController = () => {

    /** Instancia del Repository
        * @const {WebPayRepository}
        */
    const webPayRepository = WebPayRepository.instance;


    /** Obtiene respuesta del envío de orden a Webpay
        * @param transaction @interface IWebPayTransaction  Transacción con los datos para crear orden de webpay
        * @returns @interface IWebPayResponse respuesta de webpay
    */
    const createWebPayTransaction = async (transaction: IWebPayTransaction): Promise<IWebPayResponse | null> => {

        try {
            const result = await webPayRepository.createWebPayTransaction(transaction);
            return result.data;
        } catch {
            return null;
        }

    }

    /** Obtiene respuesta de la confirmación de orden a Webpay
        * @param token Token que responde la transacción 
        * @returns @interface IWebPayResponse respuesta de webpay
    */
    const commitWebPayTranstaction = async (token:string): Promise<IWebPayCommitResponse | null> =>{
        try {
            const result = await webPayRepository.commitWebPayTransaction(token);
            return result.data;
        } catch {
            return null;
        }
    }

    return {
        createWebPayTransaction,
        commitWebPayTranstaction,
    };
}

export default WebPayController;