import { NavLink, useParams } from "react-router-dom";
import { addFotosPorProducto, deleteFotos, getFotosDeProductos } from "../../../data/services/ApiRestAxios";
import { useEffect, useState } from "react";
import { Image } from "react-bootstrap";
import { useFormik } from "formik";
import Swal from "sweetalert2";

export interface IPhotos {
    id: number;
    foto: string;
}

export interface IFormularioAddPhoto {
    foto: string;
}

const AxiosProductsPhotos = () => {

    const { idProduct } = useParams();

    const product: number = Number(idProduct ?? '0')

    const [photos, setPhotos] = useState<IPhotos[] | null>(null);

    const getPhotos = async () => {
        const photoList = await getFotosDeProductos(product);
        setPhotos(photoList)
        console.log(photoList)
        return photoList
    }


    const formik = useFormik({
        initialValues: {
            foto: null  // Inicializa el campo de la foto como null
        },
        onSubmit: async (values) => {
            try {
                const result = await addFotosPorProducto(values.foto, product.toString());
                if (result == 201) {
                    Swal.fire({
                        icon: "success",
                        title: 'Wohoww',
                        html: `<u>'Imagen guardada'</u>`
                    })
                    getPhotos()
                } else {
                    Swal.fire({
                        icon: "error",
                        title: 'Uops',
                        html: `<u>'Error al subir el archivo'</u>`
                    })
                }
                // Aquí podrías manejar el resultado como necesites
            } catch (error) {
                console.error('Error al subir la foto:', error);
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>${error}</u>`
                })
            }
        }
    });

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.currentTarget.files) {
            formik.setFieldValue('foto', event.currentTarget.files[0]);
        }
    };

    const handleConfirmDelete = (id: number) => {
        Swal.fire({
            icon: "warning",
            title: '¿Estas seguro de eliminar esta categoria?',
            showCancelButton: true,
            cancelButtonText: 'NO',
            confirmButtonText: 'SI'
        }).then((result) => {
            if (result.isConfirmed) {
                handleDeleteProduct(id)
            }
        })
    }

    const handleDeleteProduct = async (id: number) => {

        const categoryList = await deleteFotos(id).then((result) => {
            if (result) {
                Swal.fire({
                    icon: "success",
                    title: 'Exito',
                    html: `<u>'Producto eliminado'</u>`
                })
                getPhotos()
            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>'tuvimos un problema con el proceso'</u>`
                })
                getPhotos()
            }
        })
        return categoryList

    }

    useEffect(() => {
        getPhotos()
    }, []);

    return (
        <>
            <div>AxiosProductsPhotos</div>
            <form onSubmit={formik.handleSubmit} encType="multipart/form-data">
                <div>
                    <label htmlFor="foto">Foto</label>
                    <input
                        id="foto"
                        name="foto"
                        type="file"
                        onChange={handleChange}
                    />
                </div>
                <hr />
                <button type="submit">Subir Foto</button>
            </form>
            {photos &&
                <table>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>foto</th>
                            <th>cargar</th>
                            <th>eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        {photos.map((photo) => (
                            <tr key={photo.id}>
                                <td>{photo.id}</td>
                                <td><Image src={photo.foto} width={150} /></td>
                                <td><NavLink to={`#`}><button>Cargar</button></NavLink></td>
                                <td><button onClick={() => handleConfirmDelete(photo.id)} >Eliminar</button></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            }
        </>
    )
}

export default AxiosProductsPhotos