import { useEffect, useState } from "react";
import { deleteProductos, getProductosByCategory } from "../../../data/services/ApiRestAxios";
import { IPaginationProduct } from "./AxiosProducts";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

const AxiosProductsByCategory = () => {

    const navigate = useNavigate()


    const [products, setProducts] = useState<IPaginationProduct | null>(null);

    const { slugCategory } = useParams();

    const category: string = slugCategory ?? ''

    const getProductsbyCategory = async () => {
        const productList = await getProductosByCategory(category);
        setProducts(productList)
        return productList
    }

    const handleConfirmDelete = (id: number) => {
        Swal.fire({
            icon: "warning",
            title: '¿Estas seguro de eliminar esta categoria?',
            showCancelButton: true,
            cancelButtonText: 'NO',
            confirmButtonText: 'SI'
        }).then((result) => {
            if (result.isConfirmed) {
                handleDeleteProduct(id)
            }
        })
    }

    const handleDeleteProduct = async (id: number) => {

        const categoryList = await deleteProductos(id).then((result) => {
            if (result) {
                Swal.fire({
                    icon: "success",
                    title: 'Exito',
                    html: `<u>'Producto eliminado'</u>`
                })
                getProductsbyCategory()
            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>'tuvimos un problema con el proceso'</u>`
                })
                navigate(`/axios/productos/productos-categoria/${category}`)
            }
        })
        return categoryList

    }

    useEffect(() => {
        getProductsbyCategory()
    }, []);

    return (
        <>
            <div>AxiosProductsByCategory</div>
            {products &&
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Slug</th>
                            <th>Categoría</th>
                            <th>Descripción</th>
                            <th>Fecha</th>
                            <th>Precio</th>
                            <th>Fotos</th>
                            <th>Stock</th>
                            <th>editar</th>
                            <th>eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        {products.datos.map((product) => (
                            <tr key={product.id}>
                                <td>{product.id}</td>
                                <td>{product.nombre}</td>
                                <td>{product.slug}</td>
                                <td>{product.categoria}</td>
                                <td>{product.descripcion}</td>
                                <td>{product.fecha}</td>
                                <td>{product.precio}</td>
                                <td>{product.stock}</td>
                                <td><NavLink to={`/axios/productos/fotos-productos/${product.id}`}><button>Ver Fotos</button></NavLink></td>
                                <td><NavLink to={`/axios/productos/edit/${product.id}`}><button>Editar</button></NavLink></td>
                                <td><button onClick={() => handleConfirmDelete(product.id)}>Eliminar</button></td>
                                </tr>
                        ))}
                    </tbody>
                </table>
            }
        </>

    )
}

export default AxiosProductsByCategory