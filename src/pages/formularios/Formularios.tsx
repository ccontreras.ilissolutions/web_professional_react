import { NavLink } from "react-router-dom";

const Formularios = () => {
    return (
        <>
            <p>Esto es Formularios</p>
            <p><NavLink to={'/formularios/formulario-simple'}>Formulario Simple</NavLink></p>
            <p><NavLink to={'/formularios/formulario-use-action-data'}>Formulario useActionData</NavLink></p>
            <p><NavLink to={'/formularios/formulario-formik'}>Formulario Formik (Top)</NavLink></p>
            <p><NavLink to={'/formularios/formulario-react-hook-form'}>Formulario React Hook Form</NavLink></p>
            <p><NavLink to={'/formularios/formulario-react-final-form'}>Formulario React Final Form</NavLink></p>
        </>
    );
}

export default Formularios;