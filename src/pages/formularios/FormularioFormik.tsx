import { useFormik } from "formik";
import Swal from "sweetalert2";

interface IFormularioFormik {
    correo: string;
    password: string
}
const FormularioFormik = () => {

   

    const { handleSubmit, handleChange, values } = useFormik({
        initialValues: {
            correo: "",
            password: ""
        },
        onSubmit: (values: IFormularioFormik) => {
            let mensaje = ''

            if (!values.correo) {
                mensaje = mensaje + "<li>Email obligatorio</li>"
            }
            if (!values.password) {
                mensaje = mensaje + "<li>contraseña obligatorio</li>"
            }
            if (mensaje === '') {
                Swal.fire({
                    icon: "success",
                    title: 'OK',
                    text: `Email: ${values.correo}`
                })
            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>${mensaje}</u>`
                })
            }
        }
    })
    return (
        <>
            <span>Esto es FormularioFormik</span>
            <form onSubmit={handleSubmit} >
                <div>
                    <label htmlFor="correo">E-mail</label>
                    <input id='correo' name='correo' value={values.correo} onChange={handleChange}></input>
                </div> <div>
                    <label htmlFor="password">Contraseña</label>
                    <input type='password' id='password' name='password' value={values.password} onChange={handleChange}></input>
                </div>
                <hr />
                <button type="submit">Enviar</button>
            </form>
        </>

    );
}

export default FormularioFormik;