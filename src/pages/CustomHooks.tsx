import { useState } from "react";
import { categorias, productos } from "../mocks/dataCustomHook";
import useCustomHooks from "../hooks/useCustomHooks";

const CustomHooks = () => {

    const [categoria, setCategoria] = useState(0);
    const [producto, setProducto] = useState('0');
    const { verduras, setVerduras } = useCustomHooks();

    const handleDesplegar = (valor: number) => {
        setCategoria(valor)
        if (valor === 0) {
            setVerduras([])
        } else {
            setVerduras(productos.filter(prod => prod.categoryId === valor))
        }
    }



    return (
        <>
            <p>Esto es CustomHooks</p>
            <div>
                <p><label htmlFor="categoria">Categorias</label></p>
                <select value={categoria} onChange={(e) => { handleDesplegar(parseInt(e.target.value)) }} id="categoria">
                    <option value={0}>selecione categoria</option>
                    {categorias.map((cat) => (
                        <option key={cat.id} value={cat.id}>{cat.nombre}</option>
                    ))}
                </select>
                <select value={producto} onChange={(e) => { setProducto(e.target.value) }} id="producto">
                    <option value={0}>selecione producto</option>
                    {verduras.map((verdura) => (
                        <option key={verdura.id} value={verdura.id}>{verdura.nombre}</option>
                    ))}
                </select>
            </div >
        </>
    );
}

export default CustomHooks;