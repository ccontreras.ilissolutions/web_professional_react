import Swal from 'sweetalert2';
import { editCategorias, getCategoriasById } from '../../../data/services/ApiRestAxios';
import { useFormik } from 'formik';
import { useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { IFormularioAddCategory } from './AxiosAddCategory';



const AxiosEditCategory = () => {

    const navigate = useNavigate()

    const { idCategory } = useParams();

    const category: number = Number(idCategory ?? '0')

    const [categoryData, setCategoryData] = useState('');

    const getCategoryById = async () => {

        const categoryList = await getCategoriasById(category).then((result) => {
            if (result) {
                setCategoryData(result.nombre)
            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>'tuvimos un problema con el proceso'</u>`
                })
                navigate('/axios/categorias')
            }
        })
        return categoryList

    }
    
    const { handleSubmit, handleChange, values, setValues } = useFormik({
        initialValues: {
            nombre: categoryData,
        },
        onSubmit: (values: IFormularioAddCategory) => {
            let mensaje = ''

            if (!values.nombre) {
                mensaje = mensaje + "<li>Nombre de categoria obligatorio</li>"
            }
            if (mensaje === '') {
                editCategorias({ nombre: values.nombre }, category).then((result) => {
                    if (result === 201) {
                        Swal.fire({
                            icon: "success",
                            title: 'Wohoww',
                            html: `<u>'Categoria editada'</u>`
                        })
                        navigate('/axios/categorias')
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: 'Uops',
                            html: `<u>'tuvimos un problema con el proceso'</u>`
                        })
                    }
                })

            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>${mensaje}</u>`
                })
            }
        }
    })

    useEffect(() => {
        getCategoryById()
        setValues({
            ...values,
            nombre: categoryData,
        });
    }, [categoryData]);

    return (
        <>
            <div>AxiosEditCategory</div>
            <form onSubmit={handleSubmit} >
                <div>
                    <label htmlFor="nombre">nombre</label>
                    <input id='nombre' name='nombre' value={values.nombre} onChange={handleChange}></input>
                </div>
                <hr />
                <button type="submit">Enviar</button>
            </form>
        </>
    )
}

export default AxiosEditCategory