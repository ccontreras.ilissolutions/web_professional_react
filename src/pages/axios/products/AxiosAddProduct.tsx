import Swal from 'sweetalert2';
import { addProductos, getCategorias } from '../../../data/services/ApiRestAxios';
import { useFormik } from 'formik';
import { useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { ICategory } from './AxiosCategory';



export interface IFormularioAddProduct {
    categoria: number;
    nombre: string;
    descripcion: string;
    precio: number;
    stock: number;
}


const AxiosAddProduct = () => {

    const navigate = useNavigate()

    const [categories, setCategories] = useState<ICategory[] | null>(null);

    const getCategory = async () => {
        const categoryList = await getCategorias();
        setCategories(categoryList)
        return categoryList
    }

    const { handleSubmit, handleChange, values } = useFormik({
        initialValues: {
            categoria: 0,
            nombre: "",
            descripcion: "",
            precio: 0,
            stock: 0,
        },
        onSubmit: (values: IFormularioAddProduct) => {
            let mensaje = ''

            if (!values.categoria || values.categoria === 0) {
                mensaje = mensaje + "<li>Debe seleccionar una categoria</li>"
            }

            if (!values.nombre) {
                mensaje = mensaje + "<li>Nombre de producto obligatorio</li>"
            }

            if (!values.nombre) {
                mensaje = mensaje + "<li>Descripcion de producto obligatorio</li>"
            }

            if (!values.precio || values.precio === 0) {
                mensaje = mensaje + "<li>Precio debe ser mayor a 0</li>"
            }

            if (mensaje === '') {
                addProductos({ categorias_id: values.categoria, nombre: values.nombre, descripcion: values.descripcion, precio: values.precio, stock: values.stock }).then((result) => {
                    if (result === 201) {
                        Swal.fire({
                            icon: "success",
                            title: 'Wohoww',
                            html: `<u>'Categoria creada'</u>`
                        })
                        navigate('/axios/productos')
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: 'Uops',
                            html: `<u>'tuvimos un problema con el proceso'</u>`
                        })
                    }
                })

            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>${mensaje}</u>`
                })
            }
        }
    })

    useEffect(() => {
        !categories && getCategory()
    }, [categories]);

    return (
        <>
            <div>AxiosAddProduct</div>
            <form onSubmit={handleSubmit} >
                <div>
                    <label htmlFor="categoria">categoria</label>
                    <select id='categoria' name='categoria' value={values.categoria} onChange={handleChange}>
                        <option value={0}>Seleccione una categoria</option>
                        {categories?.map((category => (
                            <option key={category.id} value={category.id}>{category.nombre}</option>
                        )))}
                    </select>
                </div>
                <div>
                    <label htmlFor="nombre">nombre</label>
                    <input id='nombre' name='nombre' value={values.nombre} onChange={handleChange}>

                    </input>
                </div>
                <div>
                    <label htmlFor="descripcion">descripcion</label>
                    <input id='descripcion' name='descripcion' value={values.descripcion} onChange={handleChange}>

                    </input>
                </div>
                <div>
                    <label htmlFor="precio">precio</label>
                    <input id='precio' name='precio' value={values.precio} onChange={handleChange}>

                    </input>
                </div>
                <div>
                    <label htmlFor="stock">stock</label>
                    <input id='stock' name='stock' value={values.stock} onChange={handleChange}>
                    </input>
                </div>
                <hr />
                <button type="submit">Enviar</button>
            </form>
        </>
    )
}

export default AxiosAddProduct