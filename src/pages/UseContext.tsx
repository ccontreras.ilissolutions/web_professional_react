import { useContext } from "react";
import EjemploContext from "../context/EjemploProvider";

const UseContext = () => {

    const {
        variableContext,
        saludar,
        stateContext,
        setStateContext
    } = useContext(EjemploContext)

    const handleChange = () => {
        setStateContext?.('El estado del contexto cambió a ñandú')
    }

    return (
        <>
        <span>Esto es UseContext</span>
        <p>{variableContext}</p>
        <p>{saludar?.('Juan')}</p>
        <p>{stateContext}</p>
        <button onClick={handleChange}>Cambiar contexto</button>
        </>
    );
}

export default UseContext;