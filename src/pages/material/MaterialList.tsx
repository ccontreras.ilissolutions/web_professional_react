import { Collapse, List, ListItemButton, ListItemIcon, ListItemText, ListSubheader } from "@mui/material";
import SendIcon from '@mui/icons-material/Send';
import DraftsIcon from '@mui/icons-material/Drafts';
import InboxIcon from '@mui/icons-material/Inbox';
import StarBorder from '@mui/icons-material/StarBorder';

import Swal from "sweetalert2";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import { useState } from "react";


const MaterialList = () => {

    const [open, setOpen] = useState<boolean>(false);
    
    const handleOpen = () => {setOpen(!open)}
    const handleMenu = () => {
        Swal.fire({
            icon: "success",
            title: 'OK',
            text: `enviado`
        })
    }
    return (
        <>
            <span>Esto es MaterialList</span>
            <List 
                sx={{ width: '400px', maxWidth: 600, bgcolor: 'background.paper' }} 
                component={'nav'} aria-labelledby="nested-list-subheader" 
                subheader={
                    <ListSubheader component='div' id='nested-list-subheader'>Titulo</ListSubheader>
                }> 
                <ListItemButton onClick={handleMenu}>
                    <ListItemIcon>
                        <SendIcon/>
                        <ListItemText primary="Enviar E-Mail" />
                    </ListItemIcon>
                </ListItemButton>
                <ListItemButton >
                    <ListItemIcon>
                        <DraftsIcon/>
                        <ListItemText primary="Borradores" />
                    </ListItemIcon>
                </ListItemButton>
                <ListItemButton onClick={handleOpen}>
                    <ListItemIcon>
                        <InboxIcon/>
                        <ListItemText primary="Bandeja de entrada" />
                        {open ? <ExpandLess/> : <ExpandMore/>}
                    </ListItemIcon>
                </ListItemButton>
                <Collapse in={open} timeout={'auto'} unmountOnExit>
                    <List component={'div'} disablePadding>
                    <ListItemButton >
                        <ListItemIcon sx={{pl:4}}>
                            <StarBorder/>
                            <ListItemText primary="Destacados" />
                        </ListItemIcon>
                    </ListItemButton>
                    <ListItemButton >
                        <ListItemIcon sx={{pl:4}}>
                            <StarBorder/>
                            <ListItemText primary="Archivados" />
                        </ListItemIcon>
                    </ListItemButton>
                    </List>
                </Collapse>
            </List>
        </>
    );
}

export default MaterialList;