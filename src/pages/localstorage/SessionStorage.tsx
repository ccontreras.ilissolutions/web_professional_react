import Swal from "sweetalert2";

const SessionStorage = () => {

    const handleCreateLocalStorage = ():void => {
        sessionStorage.setItem('token', '123456')
        Swal.fire({
            icon: "success",
            title: 'OK',
            text: `SessionStorage Creado`
        })
    }
    const handleDeleteLocalStorage = ():void => {
        sessionStorage.removeItem('token')
        Swal.fire({
            icon: "success",
            title: 'OK',
            text: `SessionStorage Borrado`
        })
    }

    return (
        <>
            <span>Esto es SessionStorage</span>
            <button onClick={handleCreateLocalStorage}>Crear SessionStorage</button>
            <button onClick={handleDeleteLocalStorage}>Borrar SessionStorage</button>

        </>
    );
}

export default SessionStorage;