import { Link } from "react-router-dom"

const Fetch = () => {
  return (
    <>
        <span>Esto es Fetch</span>
        <ul>
            <li>
                <Link to={"/fetch/categorias"}>Categorías</Link>
            </li>
            <li>
                <Link to={"/fetch/productos"}>Productos</Link>
            </li>
            
        </ul>
        </>
  )
}

export default Fetch