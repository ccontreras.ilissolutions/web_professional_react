import Swal from 'sweetalert2';
import { editProductos, getCategorias, getCategoriasById, getProductosById } from '../../../data/services/ApiRestAxios';
import { useFormik } from 'formik';
import { useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { ICategory } from './AxiosCategory';
import { IFormularioAddProduct } from './AxiosAddProduct';
import { IProduct } from './AxiosProducts';



const AxiosEditProduct = () => {

    const navigate = useNavigate()

    const { idProduct } = useParams();

    const product: number = Number(idProduct ?? '0')

    const [categoryData, setCategoryData] = useState('');

    const [categories, setCategories] = useState<ICategory[] | null>(null);

    const [productData, setProductData] = useState<IProduct | null>(null);

    const getCategoryById = async (category: number) => {

        const categoryList = await getCategoriasById(category).then((result) => {
            if (result) {
                setCategoryData(result.nombre)
            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>'tuvimos un problema con el proceso'</u>`
                })
            }
        })
        return categoryList

    }

    const getCategory = async () => {
        const categoryList = await getCategorias();
        setCategories(categoryList)
        return categoryList
    }

    const getProductsById = async (product: number) => {

        const productList = await getProductosById(product).then((result) => {
            if (result) {
                setProductData(result)
            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>'tuvimos un problema con el proceso'</u>`
                })
            }
        })
        return productList

    }

    const { handleSubmit, handleChange, values, setValues } = useFormik({
        initialValues: {
            categoria: 0,
            nombre: "",
            descripcion: "",
            precio: 0,
            stock: 0,
        },
        onSubmit: (values: IFormularioAddProduct) => {
            let mensaje = ''

            if (!values.categoria || values.categoria === 0) {
                mensaje = mensaje + "<li>Debe seleccionar una categoria</li>"
            }

            if (!values.nombre) {
                mensaje = mensaje + "<li>Nombre de producto obligatorio</li>"
            }

            if (!values.nombre) {
                mensaje = mensaje + "<li>Descripcion de producto obligatorio</li>"
            }

            if (!values.precio || values.precio === 0) {
                mensaje = mensaje + "<li>Precio debe ser mayor a 0</li>"
            }
            if (mensaje === '') {
                editProductos({ categorias_id: values.categoria, nombre: values.nombre, descripcion: values.descripcion, precio: values.precio, stock: values.stock }, product).then((result) => {
                    if (result === 201) {
                        Swal.fire({
                            icon: "success",
                            title: 'Wohoww',
                            html: `<u>'Producto editado'</u>`
                        })
                        navigate('/axios/productos')
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: 'Uops',
                            html: `<u>'tuvimos un problema con el proceso'</u>`
                        })
                    }
                })

            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>${mensaje}</u>`
                })
            }
        }
    })

    useEffect(() => {
        values.categoria && values.categoria !== 0 && getCategoryById(values.categoria)
        getCategory()
    }, [categoryData]);

    useEffect(() => {
        !productData && getProductsById(product)
        productData && setValues({
            ...values,
            categoria: productData?.categorias_id ?? 0,
            nombre: productData?.nombre ?? '',
            descripcion: productData?.descripcion ?? '',
            precio: productData?.precio ?? 0,
            stock: productData?.stock ?? 0
        });
    }, [productData]);

    return (
        <>
            <div>AxiosEditProduct</div>
            <form onSubmit={handleSubmit} >
                <div>
                    <label htmlFor="categoria">categoria</label>
                    <select id='categoria' name='categoria' value={values.categoria} onChange={handleChange}>
                        {productData?.categoria && <option value={productData.categorias_id}>{productData.categoria}</option>}
                        {categories?.map((category => (
                            <option key={category.id} value={category.id}>{category.nombre}</option>
                        )))}
                    </select>
                </div>
                <div>
                    <label htmlFor="nombre">nombre</label>
                    <input id='nombre' name='nombre' value={values.nombre} onChange={handleChange}>

                    </input>
                </div>
                <div>
                    <label htmlFor="descripcion">descripcion</label>
                    <input id='descripcion' name='descripcion' value={values.descripcion} onChange={handleChange}>

                    </input>
                </div>
                <div>
                    <label htmlFor="precio">precio</label>
                    <input id='precio' name='precio' value={values.precio} onChange={handleChange}>

                    </input>
                </div>
                <div>
                    <label htmlFor="stock">stock</label>
                    <input id='stock' name='stock' value={values.stock} onChange={handleChange}>
                    </input>
                </div>
                <hr />
                <button type="submit">Enviar</button>
            </form>
        </>
    )
}

export default AxiosEditProduct