import { useForm } from "react-hook-form";
import Swal from "sweetalert2";

interface IFormularioReactHookForm {
    correo: string;
    password: string
}

const FormularioReactHookForm = () => {
    const { register, formState: { errors }, handleSubmit } = useForm()
    const onSubmit = (values:IFormularioReactHookForm) => { 
        Swal.fire({
            icon: "success",
            title: 'OK',
            text: `Email: ${values.correo}`
        })
    }
    
    return (
        <>
            <span>Esto es FormularioReactHookForm</span>
            {(errors.correo || errors.password) && (
                <ul>
                    {errors.correo && <li>{errors.correo?.message as string}</li>}
                    {errors.correo && <li>{errors.password?.message as string}</li>}
                </ul>
            )}
            <form onSubmit={handleSubmit(onSubmit)} >
                <div>
                    <label htmlFor="correo">E-mail</label>
                    <input id='correo' {...register("correo", {
                        required: "el campo correo es obligatorio", 
                        pattern: {
                            value: /AZ/i,
                            message: 'Error Regex'
                        }
                    })}></input>
                </div> <div>
                    <label htmlFor="password">Contraseña</label>
                    <input type='password' id='password'  {...register("password", { required: "el campo password es obligatorio" })}></input>
                </div>
                <hr />
                <button type="submit">Enviar</button>
            </form>
        </>

    );
}

export default FormularioReactHookForm;