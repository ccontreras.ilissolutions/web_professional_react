import { Button, IconButton, Stack } from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';

const MaterialBotones = () => {
    return (
        <>
        <span>Esto es MaterialBotones</span>
        <hr></hr>
        <Stack spacing={2} direction={'row'}>
            <Button variant="text">Text</Button>
            <Button variant="contained">Text</Button>
            <Button variant="outlined">Text</Button>
        </Stack>
        <hr></hr>
        <Stack spacing={2} direction={'row'}>
            <Button variant="contained">Text</Button>
            <Button disabled>Disabled</Button>
            <Button href="#link">Link</Button>
        </Stack>
        <hr></hr>
        <Stack spacing={2} direction={'row'}>
            <Button variant="contained">Text</Button>
            <Button variant="contained" disabled>Disabled</Button>
            <Button variant="contained" href="#link">Link</Button>
        </Stack>
        <hr></hr>
        <Stack spacing={2} direction={'row'}>
            <Button variant="outlined">Text</Button>
            <Button variant="outlined" disabled>Disabled</Button>
            <Button variant="outlined" href="#link">Link</Button>
        </Stack>
        <hr></hr>
        <Stack spacing={2} direction={'row'}>
            <Button variant="contained" disableElevation>Disable elevation</Button>
        </Stack>
        <hr></hr>
        <Stack spacing={2} direction={'row'}>
            <Button  color="secondary">Secondary</Button>
            <Button variant="contained" color="success">Success</Button>
            <Button variant="outlined" color="error">Error</Button>

        </Stack>
        <hr></hr>
        <Stack spacing={2} direction={'row'} alignItems={'center'}>
            <Button variant="contained"   color="success">
                Upload
                <input hidden accept="image/*" multiple type="file"></input>
            </Button>
            <IconButton color="primary" aria-label="upload picture" >
                <i className="fas fa-camera-retro"></i>
                <input hidden accept="image/*" multiple type="file"></input>
            </IconButton>

        </Stack>
        <hr></hr>
        <Stack spacing={2} direction={'row'} alignItems={'center'}>
            <Button variant="outlined"  startIcon={<DeleteIcon/>}>
                Delete
            </Button>
            <Button variant="contained"  endIcon={<SendIcon/>}>
                Send
            </Button>
        </Stack>
        <hr></hr>
        <Stack spacing={2} direction={'row'} alignItems={'center'}>
            <IconButton aria-label="delete" >
                <DeleteIcon/>
            </IconButton>
            <IconButton color="primary" aria-label="delete"  disabled>
                <DeleteIcon/>
            </IconButton>
        </Stack>
        </>
    );
}

export default MaterialBotones;