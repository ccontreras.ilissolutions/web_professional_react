import { useEffect, useState } from 'react'
import { addCategorias, getCategorias } from '../../../data/services/ApiRestFetch';
import { NavLink } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import { IFormularioAddCategory } from '../../axios/categories/AxiosAddCategory';
import Swal from 'sweetalert2';
import { useFormik } from 'formik';



export interface ICategory {
    id: number;
    nombre: string;
    slug: string;
}

const FetchCategory = () => {

    const [categories, setCategories] = useState<ICategory[] | null>(null);

    const [show, setShow] = useState<boolean>(false);

    const handleClose = () => {
        setShow(false);
        resetForm();
    }

    const getCategory = async () => {
        const categoryList = await getCategorias();
        setCategories(categoryList)
        return categoryList
    }

    const { handleSubmit, handleChange, values, resetForm } = useFormik({
        initialValues: {
            nombre: "",
        },
        onSubmit: (values: IFormularioAddCategory) => {
            let mensaje = ''

            if (!values.nombre) {
                mensaje = mensaje + "<li>Nombre de categoria obligatorio</li>"
            }
            if (mensaje === '') {
                addCategorias({ nombre: values.nombre }).then((result) => {
                    if (result.status === 201) {
                        Swal.fire({
                            icon: "success",
                            title: 'Wohoww',
                            html: `<u>'Categoria creada'</u>`
                        })
                        handleClose()
                        getCategory()
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: 'Uops',
                            html: `<u>'tuvimos un problema con el proceso'</u>`
                        })
                    }
                })

            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>${mensaje}</u>`
                })
            }
        }
    })

    useEffect(() => {
        !categories && getCategory()
    }, [categories]);
    return (
        <>
            <div>FetchCategory</div>
            <button onClick={() => setShow(true)}>Crear +</button>
            {categories &&
                <table>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>nombre</th>
                            <th>slug</th>
                            <th>editar</th>
                            <th>eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        {categories.map((category) => (
                            <tr key={category.id}>
                                <td>{category.id}</td>
                                <td>{category.nombre}</td>
                                <td>{category.slug}</td>
                                <td><NavLink to={`/axios/categorias/edit/${category.id}`}><button>Editar</button></NavLink></td>
                                <td><button onClick={undefined}>Eliminar</button></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            }
            <Modal size='lg' show={show} onHide={handleClose} id='Modal'>
                <Modal.Header closeButton>
                    <Modal.Title>Crear Categoria</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form onSubmit={handleSubmit} >
                        <div>
                            <label htmlFor="nombre">nombre</label>
                            <input id='nombre' name='nombre' value={values.nombre} onChange={handleChange}></input>
                        </div>
                        <hr />
                        <button type="submit">Enviar</button>
                    </form>
                </Modal.Body>
            </Modal>
        </>
    )
}

export default FetchCategory