import dayjs from "dayjs";
import 'dayjs/locale/es' // load on demand


const Days = () => {
    dayjs.locale('es')
    const fecha = new Date();

    return (
        <>
        <span>Esto es Days</span>
        <h3>Formateo de fecha</h3>
        <ul>
            <li>Original: {fecha.toString()}</li>
            <li>Formato Largo:{dayjs(fecha).format('dddd')} {dayjs(fecha).format('DD')} {dayjs(fecha).format('MMMM')} {dayjs(fecha).format('YYYY')} {dayjs(fecha).format('HH.mm.ss')}</li>
            <li>Formato Corto:{dayjs(fecha).format('DD/MM/YYYY')}</li>
            <li>TimeStamp {fecha.valueOf()}</li>
        </ul>
        <h3>Calculo con fechas</h3>
        <ul>
            <li>Fecha + 7 días: {dayjs(fecha).add(7, 'day').format('DD/MM/YYYY')}</li>
            <li>Fecha - 7 días: {dayjs(fecha).subtract(7, 'day').format('DD/MM/YYYY')}</li>
            <li>Fecha + 7 meses: {dayjs(fecha).add(7, 'month').format('DD/MM/YYYY')}</li>
            <li>Fecha - 7 meses: {dayjs(fecha).subtract(7, 'month').format('DD/MM/YYYY')}</li>
            <li>Fecha + 7 años: {dayjs(fecha).add(7, 'year').format('DD/MM/YYYY')}</li>
            <li>Fecha - 7 años: {dayjs(fecha).subtract(7, 'year').format('DD/MM/YYYY')}</li>
        </ul>
        </>
    );
}

export default Days;