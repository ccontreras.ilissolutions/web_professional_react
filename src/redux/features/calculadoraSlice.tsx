import { createSlice } from "@reduxjs/toolkit";
export interface ICalculadora {
    numberA: number;
    numberB:number;
    resultado: number;
}

const initialState:ICalculadora = {
    numberA: 10,
    numberB: 10,
    resultado: 0
}

export const calculadoraSlice = createSlice({
    name: "calculadora",//cuando se importa se usa este nombre con el sufijo Reducer Ej. calculadoraReducer
    initialState,
    reducers:
    {
        sumar: (state) => {
            state.resultado = state.numberA + state.numberB
        },
        restar: (state) => {
            state.resultado = state.numberA - state.numberB
        },
        multiplicar: (state) => {
            state.resultado = state.numberA * state.numberB
        },
        dividir: (state) => {
            if (state.numberA != 0) {
                state.resultado = state.numberA / state.numberB
            }else{
                state.resultado = 0
            }
        },
        resetear:  (state) => {
            state.resultado = 0
        },

    }
})

export const {sumar, restar, multiplicar, dividir, resetear} = calculadoraSlice.actions;

export default calculadoraSlice.reducer;