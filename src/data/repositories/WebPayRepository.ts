import axios, { AxiosResponse } from 'axios';
import config from '../services/api';
import { IWebPayCommitResponse, IWebPayResponse, IWebPayTransaction } from '../../domain/interfaces/IWebPay';



/** Representa la interacción con el Back */
export default class WebPayRepository {
    /** Instancia de la clase   */
    private static _instance: WebPayRepository;

    /** Patron Singleton
      * @returns {instance} _instance WebPayRepository
    */
    static get instance(): WebPayRepository {
        if (!this._instance) {
            this._instance = new WebPayRepository();
        }
        return this._instance;
    }

    /** Recupera el inventario de productos en un template xlsx
     * @param transaction @interface IWebPayTransaction  Transacción con los datos para crear orden de webpay
     * @returns respuesta de webpay @interface IWebPayResponse
    */
    async createWebPayTransaction(transaction: IWebPayTransaction): Promise<AxiosResponse<IWebPayResponse>> {

        const url: string = '/webpay';

        return await axios.post(
            url,
            transaction,
            config,
        ).then((response) => {
            return response
        }
        ).catch((error) => {
                return error.response
        }
        );
    }

    /** Obtiene respuesta de la confirmación de orden a Webpay
        * @param token Token que responde la transacción 
        * @returns @interface IWebPayResponse respuesta de webpay
    */
    async commitWebPayTransaction(token: string): Promise<AxiosResponse<IWebPayCommitResponse>> {

        const url: string = `${config.baseURL}/webpay/commit?token_ws=${token}`;

        return await axios.post(
            url,
            config,
        ).then((response) => {
            return response
        }
        ).catch((error) => {
                return error.response
            
        }
        );
    }

}