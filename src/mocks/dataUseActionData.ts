const categorias_productos = [
    {
        id: 1,
        nombre: 'Abarrotes'
    },
    {
        id: 2,
        nombre: 'Lacteos'
    },
    {
        id:3,
        nombre: 'Carnes'
    },
    {
        id:4,
        nombre: 'Licores'
    },
    {
        id: 5,
        nombre: 'Perfumes'
    }
]


const atributos = [
    {
        id: 1,
        nombre: 'Armable'
    },
    {
        id: 2,
        nombre: 'Perecible'
    },
    {
        id: 3,
        nombre: 'Fragil'
    },
    {
        id:4,
        nombre: 'Muiltiuso'
    },
    {
        id: 5,
        nombre: 'Edicion Limitada'
    }
]

export {categorias_productos, atributos}