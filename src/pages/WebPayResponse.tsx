import { useEffect, useState } from 'react';
import WebpayController from '../controllers/WebPayController'
import { useSearchParams } from 'react-router-dom';
import { IWebPayCommitResponse } from '../domain/interfaces/IWebPay';

/** Componente que simula la respuesta que retorna Transbank*/
const WebPayResponse = () => {

    /** Hook de React Router Dom que trae los parametros de la URL */
    const [params] = useSearchParams()

    /** Respuesta del token de una transacción anulada */
    const nullToken: string | null = params.get('TBK_TOKEN')?.toString() ?? ''

    /** Respuesta del token de una transacción procesada (Autorizada o Rechazada por Transbank) */
    const processedToken: string | null = params.get('token_ws')?.toString() ?? ''

    /** Respuesta de la operación Transbank */
    const [webpayResponse, setWebpayResponse] = useState<IWebPayCommitResponse | null>(null)

    /**LLamado al controlador para confirmar transacción */
    const {
        commitWebPayTranstaction
    } = WebpayController();

    /**Función asincrona que confirma la transacción */
    const getCommitWebpayResponse = async () => {
        try {
            const apiResponse: IWebPayCommitResponse | null = await commitWebPayTranstaction(processedToken);
            if (apiResponse) {
                console.log(apiResponse)
                setWebpayResponse(apiResponse)
            } else {
                throw new Error("No se pudo obtener una respuesta válida de la transacción de WebPay.");
            }
        } catch (error) {
            throw new Error(`Error al obtener la respuesta de la transacción de WebPay`);
        }
    }

    /** UseEffect que se ejecuta para confirmar la transacción solo si viene un token procesado */
    useEffect(() => {
        processedToken !== '' && getCommitWebpayResponse()
    }, [processedToken]);

    return (
        <>
            <p>Esto es WebPayResponse</p>
            {nullToken &&
                <p>Anulaste la transacción</p>
            }
            {webpayResponse &&
                <>
                    <p> El resultado de tu operación fue: {webpayResponse.status}</p>
                    <ul>
                        <li>Orden: {webpayResponse.buyOrder}</li>
                        <li>Monto: {webpayResponse.amount}</li>
                        <li>Tarjeta: ****{webpayResponse.cardDetail?.cardNumber ?? ''}</li>
                        <li>Código de Autorización: {webpayResponse.authorizationCode}</li>
                        <li>Fecha de Operación: {webpayResponse.transactionDate}</li>
                    </ul>
                </>
            }
        </>
    );
}

export default WebPayResponse;