import { Location, useLocation } from "react-router-dom";

const QueryStringPage = () => {

    const location: Location = useLocation();

    const search = location.search

    const id: string = new URLSearchParams(search).get('id') ?? ''
    const slug: string = new URLSearchParams(search).get('slug') ?? ''

    return (
        <>
            <span>Esto es QueryStringPage</span>
            <span>{`ID: ${id}`}</span>
            <span>{`Slug: ${slug}`}</span>

        </>
    );
}

export default QueryStringPage;