import Swal from "sweetalert2";

const LocalStorage = () => {

    const handleCreateLocalStorage = ():void => {
        localStorage.setItem('token', '123456')
        Swal.fire({
            icon: "success",
            title: 'OK',
            text: `LocalStorage Creado`
        })
    }
    const handleDeleteLocalStorage = ():void => {
        localStorage.removeItem('token')
        Swal.fire({
            icon: "success",
            title: 'OK',
            text: `LocalStorage Borrado`
        })
    }

    return (
        <>
            <span>Esto es LocalStorage</span>
            <button onClick={handleCreateLocalStorage}>Crear LocalStorage</button>
            <button onClick={handleDeleteLocalStorage}>Borrar LocalStorage</button>

        </>
    );
}

export default LocalStorage;