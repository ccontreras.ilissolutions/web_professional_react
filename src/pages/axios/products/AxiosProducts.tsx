import { useEffect, useState } from "react";
import { deleteProductos, getProductos } from "../../../data/services/ApiRestAxios";
import { Link, NavLink, useLocation, useNavigate, type Location } from "react-router-dom";
import Swal from "sweetalert2";


export interface IPaginationProduct {
    datos: IProduct[];
    links: string;
    por_pagina: number;
    total: number;
}

export interface IProduct {
    categoria: string;
    categoria_slug: string;
    categorias_id: number;
    descripcion: string;
    fecha: string;
    id: number;
    nombre: string;
    precio: number;
    slug: string;
    stock: number;
}

const AxiosProducts = () => {

    const navigate = useNavigate()

    const location: Location = useLocation();

    const search = location.search

    const page: string = new URLSearchParams(search).get('page') ?? '1'

    const [products, setProducts] = useState<IPaginationProduct | null>(null);

    const getProducts = async (pageNumber: number) => {
        const productList = await getProductos(pageNumber);
        setProducts(productList)
        return productList
    }

    const handleConfirmDelete = (id: number) => {
        Swal.fire({
            icon: "warning",
            title: '¿Estas seguro de eliminar esta categoria?',
            showCancelButton: true,
            cancelButtonText: 'NO',
            confirmButtonText: 'SI'
        }).then((result) => {
            if (result.isConfirmed) {
                handleDeleteProduct(id)
            }
        })
    }

    const handleDeleteProduct = async (id: number) => {

        const categoryList = await deleteProductos(id).then((result) => {
            if (result) {
                Swal.fire({
                    icon: "success",
                    title: 'Exito',
                    html: `<u>'Producto eliminado'</u>`
                })
                getProducts(Number(page))
            } else {
                Swal.fire({
                    icon: "error",
                    title: 'Uops',
                    html: `<u>'tuvimos un problema con el proceso'</u>`
                })
                navigate('/axios/productos')
            }
        })
        return categoryList

    }

    useEffect(() => {
        getProducts(Number(page))

    }, [page]);

    return (
        <>
            <div>AxiosProducts</div>
            <button onClick={() => navigate('/axios/productos/add')}>Crear +</button>
            {products &&
                <>
                    <table>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Slug</th>
                                <th>Categoría</th>
                                <th>Descripción</th>
                                <th>Fecha</th>
                                <th>Precio</th>
                                <th>Fotos</th>
                                <th>Stock</th>
                                <th>editar</th>
                            <th>eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            {products.datos.map((product) => (
                                <tr key={product.id}>
                                    <td>{product.id}</td>
                                    <td>{product.nombre}</td>
                                    <td>{product.slug}</td>
                                    <td><Link to={`/axios/productos/productos-categoria/${product.categoria_slug}`}>{product.categoria}</Link></td>
                                    <td>{product.descripcion}</td>
                                    <td>{product.fecha}</td>
                                    <td>{product.precio}</td>
                                    <td>{product.stock}</td>
                                    <td><NavLink to={`/axios/productos/fotos-productos/${product.id}`}><button>Ver Fotos</button></NavLink></td>
                                    <td><NavLink to={`/axios/productos/edit/${product.id}`}><button>Editar</button></NavLink></td>
                                    <td><button onClick={() => handleConfirmDelete(product.id)}>Eliminar</button></td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example">
                        <ul className="pagination">
                            {[...Array(Math.ceil(products.total / products.por_pagina))].map((_, index) => {
                                const currentPage: number = index + 1;
                                return (<li key={index} className="page-item"><Link to={`/axios/productos?page=${currentPage}`} className={`page-link ${page === (currentPage).toString() ? 'active' : ''}`} >{currentPage}</Link></li>
                                )
                            }
                            )}

                        </ul>
                    </nav>
                </>
            }
        </>
    )
}

export default AxiosProducts