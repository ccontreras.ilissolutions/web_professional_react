import { NavLink } from "react-router-dom";

const routes = [
  { path: '/custom-hooks', label: 'Custom Hooks' },
  { path: '/use-loader-data', label: 'UseLoaderData' },
  { path: '/use-ref', label: 'UseRef' },
  { path: '/webpay', label: 'WebPay' },
  { path: '/paypal', label: 'Paypal' },
  { path: '/formularios', label: 'Formularios' },
  { path: '/utils', label: 'Utils' },
  { path: '/material', label: 'Material' },
  { path: '/storage', label: 'Storage' },
  { path: '/context', label: 'UseContext' },
  { path: '/redux', label: 'Redux Ejemplo' },
  { path: '/axios', label: 'Axios' },
  { path: '/fetch', label: 'Fetch' },
];

const Home = () => {
  return (
    <>
      <p>Esto es Home</p>
      {routes.map((route, index) => (
        <p key={index}>
          <NavLink to={route.path}>{route.label}</NavLink>
        </p>
      ))}
    </>
  );
};

export default Home;