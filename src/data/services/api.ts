import axios, { AxiosRequestConfig } from 'axios';

/**
 * Guarda la configuración principal del llamado axios
 * @const {object}
 */
const config = {
    baseURL: 'http://localhost:8080',
    headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
    },
} as AxiosRequestConfig;

axios.interceptors.response.use(
    response => response,
    error => {
        return Promise.reject(error);
    }
);

export default config;