import { useState } from "react";

interface IProductos {
    id: number,
    categoryId: number,
    nombre: string,
    precio: number
}

const useCustomHooks = () => {
    const [verduras, setVerduras] = useState<IProductos[]>([]);
    //const [texto, setTexto] = useState('');

    return { verduras, setVerduras }
}

export default useCustomHooks;