import { createSlice } from "@reduxjs/toolkit";
export interface IProcedencia {
    country: string;
    city:string;
}

const initialState:IProcedencia = {country: "Venezuela", city: "Maracaibo"}

export const procedenciaSlice = createSlice({
    name: "procedencia",//cuando se importa se usa este nombre con el sufijo Reducer Ej. procedenciaReducer
    initialState,
    reducers:
    {
        cambiarMexico: (state) => {
            state.country="Mexico"
            state.city="Ciudad de Mexico"
        },
        cambiarPeru: (state) => {
            state.country="Peru"
            state.city="Lima"
        },
        cambiarChile: (state) => {
            state.country="Chile"
            state.city="Santiago"
        },
        volverVenezuela: (state) => {
            state.country="Venezuela"
            state.city="Maracaibo"
        }

    }
})

export const {cambiarChile, cambiarPeru, cambiarMexico, volverVenezuela} = procedenciaSlice.actions;

export default procedenciaSlice.reducer;