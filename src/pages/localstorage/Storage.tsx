import { NavLink } from "react-router-dom";

const Storage = () => {
    return (
        <>
        <span>Esto es Storage</span>
        <p><NavLink to={'/storage/localstorage'}>LocalStorage</NavLink></p>
        <p><NavLink to={'/storage/sessionstorage'}>SessionStorage</NavLink></p>
        </>
    );
}

export default Storage;