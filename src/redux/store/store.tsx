import { combineReducers, configureStore } from "@reduxjs/toolkit";
import procedenciaReducer from "../features/procedenciaSlice";
import calculadoraReducer from "../features/calculadoraSlice";
import parametrosReducer from "../features/parametrosSlice";


const rootReducer = combineReducers({
    procedencia: procedenciaReducer,
    calculadora: calculadoraReducer,
    parametros: parametrosReducer,
    // Agrega otros reducers aquí si tienes más
  });
  
  export type RootState = ReturnType<typeof rootReducer>;
  
  const store = configureStore({
    reducer: rootReducer,
  });

  export default store;
