import { Box, Button, Divider, Drawer, List, ListItem, ListItemIcon, ListItemText } from "@mui/material";
import { useState } from "react";
import MailIcon from '@mui/icons-material/Mail';
import InboxIcon from '@mui/icons-material/Inbox';

const MaterialDrawer = () => {

    interface IShowMenu {
        top: boolean,
        left: boolean,
        bottom: boolean,
        rigth: boolean
    }

    const initialShowMenu = {
        top: false,
        left: false,
        bottom: false,
        rigth: false
    }

    const [showMenu, setShowMenu] = useState<IShowMenu>(initialShowMenu);

    const toggleDrawer = (anchor: string, open: boolean) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setShowMenu({ ...showMenu, [anchor]: open })
    }

    const list = (anchor:string):JSX.Element => (
        <Box sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }} 
        role='presentation' 
        onClick={toggleDrawer(anchor, false)} 
        onKeyDown={toggleDrawer(anchor, false) }>
            <List>
                <ListItem disablePadding>
                    <ListItemIcon>
                        <MailIcon/>
                    </ListItemIcon>
                    <ListItemText primary='Home'/>
                </ListItem>
                <ListItem disablePadding>
                    <ListItemIcon>
                        <InboxIcon/>
                    </ListItemIcon>
                    <ListItemText primary='Nosotros'/>
                </ListItem>
            </List>
            <Divider/>
        </Box>
    )


    return (
        <>
            <span>Esto es MaterialDrawer</span>
            <Button onClick={toggleDrawer('left', true)}>Abrir</Button>
            <Drawer
                anchor="left"
                open={showMenu['left']}
                onClose={toggleDrawer('left', false)}
            >
                {list('left')}
            </Drawer>
        </>
    );
}

export default MaterialDrawer;