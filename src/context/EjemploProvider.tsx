import { PropsWithChildren, createContext, useMemo, useState } from "react";

export interface IEjemplo {
    variableContext?: string;
    saludar?: (name: string) => string
    stateContext?:string
    setStateContext?:React.Dispatch<React.SetStateAction<string>>
}
const EjemploContext = createContext<IEjemplo>({} as IEjemplo);
const EjemploProvider = ({ children }: PropsWithChildren<IEjemplo>) => {
    const [stateContext, setStateContext] = useState<string>('Estado inicial del context');
    const context: IEjemplo = useMemo(() => ({
        variableContext: 'hola Carlos', 
        saludar: (name) => name,
        stateContext,
        setStateContext
    }), [stateContext]);

    return (
        <EjemploContext.Provider value={context}>
            {children}
        </EjemploContext.Provider>
    )

}

export { EjemploProvider }

export default EjemploContext