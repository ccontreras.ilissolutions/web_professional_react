import { SyntheticEvent, useState } from "react";
import  ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Accordion, AccordionDetails, AccordionSummary, Typography } from "@mui/material";

const MaterialAccordion = () => {
    const [expanded, setExpanded] = useState<boolean | string>(false);
    const handleChange = (panel:string) => (event: SyntheticEvent, isExpanded:boolean) => {
      event.preventDefault()
      setExpanded(isExpanded ? panel : false)
    }
    return (
        <>
            <span>Esto es MaterialAccordion</span>
            <Accordion expanded={expanded ==='panel1'} onChange={handleChange('panel1')}>
              <AccordionSummary expandIcon={<ExpandMoreIcon/>}  aria-controls="panel1bh-content" id="panel1bh-header">
                <Typography variant="h4" sx={{width:'200px', flexShrink:0}}>
                  PHP
                </Typography>
                <Typography  sx={{color:'blue'}}>
                  echo "Hola Mundo;"
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/2560px-PHP-logo.svg.png" alt='test' width="250" />
                <hr></hr>
                <Typography>
                  Esto es PHP
                </Typography>
                </AccordionDetails>
            </Accordion>
        </>
    );
}

export default MaterialAccordion;